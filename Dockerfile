FROM ubuntu:rolling
RUN apt update
RUN apt upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt install -y gfortran-12 git wget valgrind
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 100 --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-12
RUN git clone https://github.com/fortran-lang/fpm.git
RUN cd fpm && chmod +x ./install.sh && ./install.sh --prefix=/
RUN apt update && apt install -y locales && rm -rf /var/lib/apt/lists/* && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8
RUN cd .. && apt update && apt install make
